#!/bin/bash

# Kavan M said he made a bit of a mess

find . -type f -maxdepth 1 -exec echo Pi > {} \;

# There are always other ways.
# This was is about the fastest way to find the files - the biggest overhead is in the repeated exec for each file.
